# terminate

The terminal emulator to end all terminals emulators: an extremely minimal
terminal primarily meant to be used with tmux.

You probably won't like its biggest feature: the lack of most features other
terminal emulators have, while containing some features I want.


## Building

Install dependencies:

- gcc
- meson
- ninja
- vte3
- vte-common
- scdoc

To build:

        $ meson --prefix=/usr builddir

        $ ninja -C builddir

To install:

        # ninja -C builddir install


Alternatively, if you are on Arch Linux, this application is on [AUR](https://aur.archlinux.org/packages/terminate)

## Usage

        $ man 1 terminate

## Configuration

There is a default config file in this project which can either be installed to `/etc/terminate/config` to apply to all users, or copied to `$XDG_CONFIG_HOME/terminate/config` and modified. `$XDG_CONFIG_HOME` takes precendent over `/etc/terminate`.
