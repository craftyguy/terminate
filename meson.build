project('terminate', 'c',
  version: '0.5',
  license: 'GPLv3',
  default_options : ['buildtype=release', 'c_std=c11']
  )

cc = meson.get_compiler('c')

vtedep = dependency('vte-2.91')

conf_data = configuration_data()
conf_data.set('version', meson.project_version())
conf_data.set('sysconfdir', get_option('sysconfdir'))

configure_file(
  input: 'terminate.h.in',
  output: '@BASENAME@',
  configuration: conf_data,
  )

executable('terminate', 'terminate.c',
  dependencies: vtedep,
  install: true,
  install_dir: 'bin',
  )

install_data(
	'config',
	install_dir: join_paths(get_option('prefix'), get_option('sysconfdir'), 'terminate')
)

scdoc = dependency('scdoc')
if scdoc.found()
	scdoc_prog = find_program(scdoc.get_pkgconfig_variable('scdoc'), native: true)
	sh = find_program('sh', native: true)
	mandir = get_option('mandir')

	man_files = [
		'doc/terminate.1.scd',
		'doc/terminate.5.scd'
	]
	foreach filename : man_files
		topic = filename.split('.')[-3].split('/')[-1]
		section = filename.split('.')[-2]
		output = '@0@.@1@'.format(topic, section)

		custom_target(
			output,
			input: filename,
			output: output,
			command: [
				sh, '-c', '@0@ < @INPUT@ > @1@'.format(scdoc_prog.path(), output)
			],
			install: true,
			install_dir: '@0@/man@1@'.format(mandir, section)
		)
	endforeach
endif
