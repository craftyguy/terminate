/*
 * Copyright(c) 2019-2021 by craftyguy "Clayton Craft" <clayton@craftyguy.net>
 *   Distributed under GPLv3+ (see LICENSE) WITHOUT ANY WARRANTY.
 */

#include "terminate.h"
struct ConfigOptions config;

int get_config(struct ConfigOptions *config){
	GKeyFile *config_file = NULL;
	gchar *color_name = NULL;
	gchar *color_val = NULL;
	gboolean loaded;
	config_file = g_key_file_new();

	config->color_fg = g_malloc(sizeof(GdkRGBA));
	config->color_bg = g_malloc(sizeof(GdkRGBA));
	config->colors16 = g_malloc(16 * sizeof(GdkRGBA));

	// Use user config dir first
	gchar *conf_path = g_strdup_printf("%s/terminate/config",
					   g_get_user_config_dir());
	loaded = g_key_file_load_from_file(config_file, conf_path,
					   G_KEY_FILE_NONE, NULL);
	// Search system config dir(s)
	for (const char *const *dir = g_get_system_config_dirs();
	     *dir && !loaded; dir++){
		conf_path = g_strdup_printf("%s/terminate/config", *dir);
		loaded = g_key_file_load_from_file(config_file, conf_path,
						   G_KEY_FILE_NONE, NULL);
	}
	// Search system config dir set at compile time
	if (!loaded){
		conf_path = g_strdup_printf("%s/terminate/config",
					    SYSCONFDIR);
		loaded = g_key_file_load_from_file(config_file, conf_path,
						   G_KEY_FILE_NONE, NULL);
	}

	g_free(conf_path);
	if (!loaded){
		g_critical("ERROR: Unable to find config file!");
		return -1;
	}

	for (int i=0; i<16; i++){
		color_name = g_strdup_printf("color%i", i);
		if ((color_val = g_key_file_get_string(config_file, "Colors",
						      color_name, NULL))){
			gdk_rgba_parse((&config->colors16[i]), color_val);
			g_free(color_val);
		}
		g_free(color_name);
	}
	if ((color_val = g_key_file_get_string(config_file, "Colors",
					     "color_fg", NULL))){
		gdk_rgba_parse(config->color_fg, color_val);
		g_free(color_val);
	}
	if ((color_val = g_key_file_get_string(config_file, "Colors",
					     "color_bg", NULL))){
		gdk_rgba_parse(config->color_bg, color_val);
		g_free(color_val);
	}

	config->font_size = g_key_file_get_double(config_file, "Settings",
						   "font_size", NULL);
	if (config->font_size == 0.0)
		config->font_size = 12.0;
	config->scrollback_buffer = g_key_file_get_integer(config_file,
							"Settings",
							"scrollback_buffer",
							NULL);
	config->font = g_key_file_get_string(config_file, "Settings",
					     "font", NULL);

	config->bell_command = g_key_file_get_string(config_file, "Settings",
						 "bell_command", NULL);

	config->url_clickable = g_key_file_get_boolean(config_file, "Settings",
						       "url_clickable", NULL);

	config->browser = g_key_file_get_string(config_file, "Settings",
						"browser", NULL);

	g_key_file_free(config_file);
	return 0;
}

int get_font_size(VteTerminal *term){
	PangoFontDescription *font_descr;
	if (!(font_descr = pango_font_description_copy(vte_terminal_get_font(term))))
		return -1;
	int size = pango_font_description_get_size(font_descr);
	pango_font_description_free(font_descr);
	return size / PANGO_SCALE;
}

static void set_font_size(VteTerminal *term, int size){
	PangoFontDescription *font_descr;
	if (!(font_descr = pango_font_description_copy(vte_terminal_get_font(term))))
		return;
	pango_font_description_set_size(font_descr, size * PANGO_SCALE);
	vte_terminal_set_font(term, font_descr);
	pango_font_description_free(font_descr);
	return;
}

static gboolean key_press(VteTerminal *term, GdkEventKey *evt, gpointer user_data){
	switch (evt->state & (GDK_CONTROL_MASK | GDK_SHIFT_MASK | GDK_MOD1_MASK)){
	case GDK_CONTROL_MASK | GDK_SHIFT_MASK:
		switch(evt->keyval){
		case GDK_KEY_V:
			vte_terminal_paste_clipboard(VTE_TERMINAL(term));
			return TRUE;
		case GDK_KEY_C:
			vte_terminal_copy_clipboard_format(VTE_TERMINAL(term),
							   VTE_FORMAT_TEXT);
			return TRUE;
		case GDK_KEY_plus:
			set_font_size(term, get_font_size(term) + 1);
			return TRUE;
		case GDK_KEY_underscore:
			set_font_size(term, get_font_size(term) - 1);
			return TRUE;
		case GDK_KEY_BackSpace:
			set_font_size(term, 0);
			return TRUE;
		}
	}
	return FALSE;
}

static void exit_app(VteTerminal *term) {
	g_print("Goodbye!\n");
	gtk_main_quit();
	exit(EXIT_SUCCESS);
}

static void execute_on_bell(VteTerminal *term, gpointer user_data){
	if (!config.bell_command)
		return;
	gchar **env = g_get_environ();
	gchar **cmd = NULL;
	g_autofree gchar *param = g_strdup_printf(config.bell_command,
						  g_get_prgname());

	if (g_shell_parse_argv(param, NULL, &cmd, NULL) == FALSE){
		g_print("Failed to parse bell command %s\n",
			config.bell_command);
		return;
	}
	g_spawn_async(NULL, cmd, env, G_SPAWN_SEARCH_PATH, NULL, NULL,
				 NULL, NULL);
	g_strfreev(env);
	g_strfreev(cmd);
}

static void term_spawn_async_cb(VteTerminal *term, GPid pid, GError *error,
				gpointer user_data){
	if (error != NULL)
		g_critical ("ERROR: Failed to set up terminal: %s",
			    error->message);
	else
		g_debug ("Terminal set up with pid: %d", pid);
}


gboolean button_press(VteTerminal *term, GdkEventButton *evt,
		      struct ConfigOptions *config){
	g_autofree gchar *url = NULL;
	if (config->url_clickable && evt->type == GDK_BUTTON_PRESS) {
		int tag;
		url = vte_terminal_match_check_event(term, (GdkEvent*) evt,
						       &tag);
		if (!url) {
			url = vte_terminal_hyperlink_check_event(term,
								   (GdkEvent*)evt);
			if (!url)
				return FALSE;
		}
		if (evt->button == 1) {
			g_debug ("Mouse click on url: %s", url);
			gchar **cmd = NULL;
			gchar **env = g_get_environ();
			g_autofree gchar *browser;

			if (!config->browser) {
				browser = g_strdup(g_environ_getenv(env, "BROWSER"));
			 } else {
				browser = g_strdup(config->browser);
			}
			g_autofree gchar *execute = g_strdup_printf("%s %s",
								    browser,
								    url);
			if (g_shell_parse_argv(execute, NULL, &cmd, NULL) == FALSE){
				g_error ("Failed to open browser");
				g_strfreev(env);
				g_strfreev(cmd);
				return FALSE;
			}
			g_spawn_async(NULL, cmd, env, G_SPAWN_SEARCH_PATH, NULL,
				      NULL, NULL, NULL);
			g_strfreev(env);
			g_strfreev(cmd);
		}
		return TRUE;
	}
	return FALSE;
}

int main(int argc, gchar **argv){
	GtkWidget *win, *term;
	PangoFontDescription *font_descr;
	gtk_init(&argc, &argv);
	term = vte_terminal_new();
	win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	int c;
	gchar **cmd = NULL;
	g_autofree gchar *execute = NULL;
	g_autofree gchar *app_id = NULL;
	g_autofree gchar *app_title = NULL;
	gchar **env = g_get_environ();

	while ((c = getopt(argc, argv, "a:e:ht:v")) != -1)
		switch(c){
		case 'a':
			app_id = g_strdup(optarg);
			break;
		case 'v':
			g_print("Terminate version %s\n", VERSION);
			return 0;
		case 'e':
			execute = g_strdup(optarg);
			break;
		case 't':
			app_title = g_strdup(optarg);
			break;
		default:
			// TODO
			g_print("Help!\n");
			return 0;
		}
	if (!app_id){
		app_id = g_strdup("terminate");
	}
	if (!app_title){
		app_title = g_strdup("terminate");
	}
	/*
	 * This configures the app_id on Wayland compositors
	 */
	g_set_prgname(app_id);

	if (get_config(&config) != 0)
		return -1;
	vte_terminal_set_scrollback_lines(VTE_TERMINAL(term),
					  config.scrollback_buffer);
	vte_terminal_set_mouse_autohide(VTE_TERMINAL(term), TRUE);
	gtk_window_set_title(GTK_WINDOW(win), app_title);

	vte_terminal_set_colors(VTE_TERMINAL(term), config.color_fg,
				config.color_bg, config.colors16, 16);
	if (!execute)
		execute = g_strdup(g_environ_getenv(env, "SHELL"));
	g_shell_parse_argv(execute, NULL, &cmd, NULL);

	if (!(font_descr = pango_font_description_from_string(config.font))){
		g_critical("Error: font not found: %s.", config.font);
	}
	pango_font_description_set_size(font_descr,
					config.font_size * PANGO_SCALE);
	vte_terminal_set_font(VTE_TERMINAL(term), font_descr);
	pango_font_description_free(font_descr);

	if (config.url_clickable){
		VteRegex *term_url_re = vte_regex_new_for_match(url_regex,
							    (gssize) strlen(url_regex),
							    PCRE2_MULTILINE | PCRE2_NOTEMPTY,
							    NULL);
		int url_match = vte_terminal_match_add_regex(VTE_TERMINAL(term),
							     term_url_re, 0);
		vte_terminal_match_set_cursor_name(VTE_TERMINAL(term),
						   url_match, "hand");
	}

	vte_terminal_spawn_async(VTE_TERMINAL(term), VTE_PTY_DEFAULT, NULL,
				 cmd, env, G_SPAWN_SEARCH_PATH, NULL, NULL,
				 NULL, -1, NULL, term_spawn_async_cb, NULL);

	g_free(config.color_fg);
	g_free(config.color_bg);
	g_free(config.colors16);
	g_free(config.font);
	g_strfreev(env);

	g_signal_connect(win, "delete-event", G_CALLBACK(exit_app), NULL);
	g_signal_connect(win, "destroy", G_CALLBACK(exit_app), NULL);
	g_signal_connect(term, "child-exited", G_CALLBACK(exit_app), NULL);
	g_signal_connect(term, "key-press-event", G_CALLBACK(key_press),
			 GTK_WINDOW(win));
	g_signal_connect(term, "button-press-event", G_CALLBACK(button_press),
			 &config);
	g_signal_connect(term, "bell", G_CALLBACK(execute_on_bell),
			 GTK_WINDOW(win));

	gtk_container_add(GTK_CONTAINER(win), term);
	gtk_widget_show_all(win);
	gtk_main();
}
