/*
* Copyright (C) 2013 Daniel Micay
*
* This is free software; you can redistribute it and/or modify it under
* the terms of the GNU Library General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU Library General Public
* License along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef URL_REGEX_H
#define URL_REGEX_H

#define USERCHARS       "-[:alnum:]"
#define USERCHARS_CLASS "[" USERCHARS "]"
#define PASSCHARS_CLASS "[-[:alnum:]\\Q,?;.!%$^*&~\"#'\\E]"
#define HOSTCHARS_CLASS "[-[:alnum:]]"
#define HOST            "(?:" HOSTCHARS_CLASS "+(\\." HOSTCHARS_CLASS "+)*)?"
#define PORT            "(?:\\:[[:digit:]]{1,5})?"
#define SCHEME          "(?:[[:alpha:]][+-.[:alnum:]]*:)"
#define USERPASS        USERCHARS_CLASS "+(?:\\:" PASSCHARS_CLASS "+)?"
#define URLPATH         "(?:/[[:alnum:]\\Q-_.!~*'();/?:@&=+$,#%\\E]*)?"

const char * const url_regex = SCHEME "//(?:" USERPASS "\\@)?" HOST PORT URLPATH;

#endif
